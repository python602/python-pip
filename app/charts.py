from os import path, makedirs
import matplotlib.pyplot as plt

imgs = f'{path.dirname(__file__)}/imgs'


def generate_bar_chart(name, labels, values):
    check_img_dir()
    fig, ax = plt.subplots()
    ax.bar(labels, values)
    plt.savefig(f'{imgs}/{name}.png')
    plt.close()


def generate_pie_chart(labels, values):
    check_img_dir()
    fig, ax = plt.subplots()
    ax.pie(values, labels=labels)
    ax.axis('equal')
    plt.savefig(f'{imgs}/pie.png')
    plt.close()


def check_img_dir():
    if not path.exists(imgs):
        print('llega')
        makedirs(imgs)


if __name__ == '__main__':
    labels = ['a', 'b', 'c']
    values = [10, 40, 800]
    # generate_bar_chart(labels, values)
    generate_pie_chart(labels, values)
