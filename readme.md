# Curso de Python: PIP y Entornos Virtuales

Curso de [platzi](https://platzi.com/cursos/python-pip/)

## Game Project

Como ejecutar el juego:

```sh
cd game
python3 main.py
```

## App Project
Puedes correr la aplicacion desde tu maquina ejecutando

```sh
git clone
cd app
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python main.py
```

o desde docker ejecutando

```sh
cd app
docker-compose build
docker-compose up -d
```

![Pie Chart Image](./app/imgs/pie.png)

## Web server
```sh
cd web-server
docker-compose build
docker-compose up -d
```